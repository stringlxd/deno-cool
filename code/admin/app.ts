import { Application,oakCors } from "./dep.ts";
import {router} from "./routers.ts";
import "./imported.ts";
import "./wechat.ts";
const app = new Application();
// Logger
app.use(async (ctx, next) => {
    await next();
    const rt = ctx.response.headers.get("X-Response-Time");
    console.log(`${ctx.request.method} ${ctx.request.url} - ${rt}`);
});
app.use(oakCors());
// Timing
app.use(async (ctx, next) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.response.headers.set("X-Response-Time", `${ms}ms`);
});
app.use(router.routes());
app.use(router.allowedMethods());

console.log("开始监听");
//这里的端口可以不填 只有在deno运行时底下是生效的 在当前的这个魔改后的版本是无效的
app.listen({ port: 3000 });
