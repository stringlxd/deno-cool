import { Context } from "../dep.ts";
const worker = new Worker(
    new URL("../worker.ts", import.meta.url).href,
    {
        type: "module",
    },
);

let sendWorkerMessage =(context:Context) => {
    worker.postMessage({ text: "hello worker" });
    context.response.body = { msg: "success" };
};
export {sendWorkerMessage};