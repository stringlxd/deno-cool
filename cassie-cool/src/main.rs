use std::{collections::HashMap, sync::Mutex};

use actix_governor::{Governor, GovernorConfigBuilder};
use actix_web::{middleware, web, App, HttpServer};
use awc::Client;
use cassie_cool::{api::api_routers, forward, proxy_ws_request};
///网关入口0
#[tokio::main]
async fn main() -> std::io::Result<()> {
  env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));
  //在这里写 是所有线程共享
  let file_table: web::Data<Mutex<HashMap<String, String>>> = web::Data::new(Mutex::new(HashMap::new()));
  bannder();
  let governor_conf = GovernorConfigBuilder::default().per_millisecond(200).burst_size(10).use_headers().finish().unwrap();
  log::info!("starting main HTTP server at http://0.0.0.0:9999");
  HttpServer::new(move || {
    //在这里写  是有问题的  只会在当前线程里有效
    App::new()
      .configure(api_routers)
      .route("/ws", web::get().to(proxy_ws_request))
      .app_data(file_table.clone())
      .app_data(web::Data::new(Client::default()))
      .wrap(middleware::Logger::default())
      .default_service(web::to(forward).wrap(Governor::new(&governor_conf)))
  })
  .bind(("0.0.0.0", 9999))?
  .run()
  .await
}
fn bannder() {
  eprintln!(
    r#"  ______                _          _____                        ______            _ 
 / _____)              (_)        (____ \                      / _____)          | |
| /      ____  ___  ___ _  ____    _   \ \ ____ ____   ___    | /      ___   ___ | |
| |     / _  |/___)/___) |/ _  )  | |   | / _  )  _ \ / _ \   | |     / _ \ / _ \| |
| \____( ( | |___ |___ | ( (/ /   | |__/ ( (/ /| | | | |_| |  | \____| |_| | |_| | |
 \______)_||_(___/(___/|_|\____)  |_____/ \____)_| |_|\___/    \______)___/ \___/|_|
"#
  );
}
